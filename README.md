# Install comments for ROS on Manjaro

## Things to add to .bashrc

1. Add the workspace source: `alias ros_ws="source devel/setup.bash"`
2. Add the OS (only if needed): `export ROS_OS_OVERRIDE=arch:$(uname -r)`

## Install ROS

Using the install scripts.

## Other operations

1. Update rosdep: `sudo rosdep init` and then `rosdep update`

---

# Node manager install

Using the install scripts.

---

# General information

## How to make a new workspace with catkin

1. Create the folder of the workspace: `mkdir -p ~/my_workspace/src`
2. Go to the workspace: `cd ~/my_workspace`
3. Generate the environment: `catkin init`
4. Set the config for the workspace
    1. Use `catkin config` to see config
    2. Use `catkin config --extend <PATH>` to extend a workspace
4. Test build the workspace
    1. Go to workspace top: `cd ~/my_workspace`
    2. Run make: `carkin build`
    3. Or with debug: `catkin build --cmake-args -DCMAKE_BUILD_TYPE=Debug`

Optionally (oneliner for configuration):
* `catkin config --init --mkdirs --extend /opt/ros/indigo --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Debug`

## How to make a new package with catkin

1. Go to the workspace source folder: `cd ~/my_workspace/src`
2. Run `catkin create pkg <package_name> --catkin-deps [dependencies]`
3. Ex: `catkin create pkg pkg_d --catkin-deps pkg_a pkg_b`
4. To enable C++11 support add `set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")` to the packge's `CMakeLists.txt`

## When adding dependencies or messages (CMakeLists.txt / package.xml)

1. Update the package.xml with new dependencies.
2. Update the CMakeLists.txt:
    1. Add dependency to `find_package(...`
    2. Add message files to `add_message_files(...`
    3. Add service files to `add_service_files(...`
    4. Add to `generate_messages(...` the messages that we are dependent on.

## When adding compilation sources to CMakeLists.txt
```
add_executable(talker src/talker.cpp)
target_link_libraries(talker ${catkin_LIBRARIES})
add_dependencies(talker beginner_tutorials_generate_messages_cpp)
```


## Publisher/Subscriber vs. Server/Client

* Publisher/Subscriber
    * One way message passing.
    * Used in simple cases.

* Server/Client
    * Two way message passing.
    * Client sends a request and a server answers it.

